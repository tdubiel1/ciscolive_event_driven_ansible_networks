# Ansible-Event-Driven-Network-Demos
The following demo leverages the Ansible Event Driven Automation. Please note the EDA is tech preview and this repo will update as new versions of EDA are released.
* No shut ports

## Getting started
The following links will explain how to install EDA and the other components used in the demo.

### Event-Driven Ansible install:
Please note this code is the developer tech preview.
https://github.com/ansible/event-driven-ansible

### Telegraf Collector Install
https://docs.influxdata.com/telegraf/v1.21/introduction/installation/

Please see this repo for my example telegraf.toml file.

### Kafka Install
https://kafka.apache.org/quickstart

